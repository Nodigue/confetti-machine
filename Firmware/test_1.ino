#include <Servo.h>

constexpr int GREEN_LED_PIN = 2;
constexpr int RED_LED_PIN = 3;
constexpr int BUTTON_INPUT_PIN = 4;

constexpr int LEFT_SERVO_PIN = 5;
constexpr int RIGHT_SERVO_PIN = 7;

Servo left_servo;
Servo right_servo;

int currentState = 0;
int maxState = 3;

int buttonPressed = 0;
bool flag = false;

void setup()
{
  pinMode(RED_LED_PIN, OUTPUT);
  pinMode(GREEN_LED_PIN, OUTPUT);
  pinMode(BUTTON_INPUT_PIN, INPUT);

  digitalWrite(RED_LED_PIN, 1);
  digitalWrite(GREEN_LED_PIN, 1);

  left_servo.attach(LEFT_SERVO_PIN);
  right_servo.attach(RIGHT_SERVO_PIN);

  Serial.begin(9600);
}

void stateMachine(int p_state)
{
  switch (p_state)
  {
    case 0:
      digitalWrite(GREEN_LED_PIN, 1);
      digitalWrite(RED_LED_PIN, 0);
      left_servo.writeMicroseconds(0);
      //right_servo.write(0);
      break;
    
    case 1:
      digitalWrite(GREEN_LED_PIN, 0);
      digitalWrite(RED_LED_PIN, 0);
      left_servo.write(180);
      //right_servo.write(180);
      break;    
  
    case 2:
    {
      digitalWrite(GREEN_LED_PIN, 0);
      digitalWrite(RED_LED_PIN, 1);
      break;
    }

    default:
      break;
  }
}



void loop()
{
  buttonPressed = digitalRead(BUTTON_INPUT_PIN);

  if (buttonPressed == HIGH && !flag)
  {
    currentState = (currentState < maxState - 1) ? currentState + 1 : 0;
    flag = true;
  }
  else if (buttonPressed == LOW)
  {
    flag = false;
  }

  Serial.print("buttonPressed:");
  Serial.print(buttonPressed);
  Serial.print(",");
  Serial.print("currentState:");
  Serial.print(currentState);
  Serial.print(",");
  Serial.print("flag:");
  Serial.println(flag);

  stateMachine(currentState);

  delay(15);
}
