#!/usr/bin/env python3

import requests
import time

# GitLab API base URL and personal access token
BASE_URL = "https://gitlab.com/api/v4"  # Replace with your GitLab instance URL
ACCESS_TOKEN = "glpat-mGUFtXSzSFrpHCDfc8ti"  # Replace with your GitLab personal access token

# Your user ID on GitLab (replace with your own user ID)
USER_ID = 2948509

def get_project_name(project_id : str) -> str:
    """
    Fetch the name of the project name from its id.
    """

    if project_id is None:
        return "Any project ID"

    url = f"{BASE_URL}/projects/{project_id}"
    headers = {"Authorization": f"Bearer {ACCESS_TOKEN}"}

    try:
        response = requests.get(url, headers=headers)
        response.raise_for_status()
        project_data = response.json()

        return project_data.get('name', 'Unknown projects')

    except requests.exceptions.RequestException as e:
        print(f"Error fetching data: {e}")
        return "Error"

def get_open_merge_requests() -> list:
    """
    Fetch the gitlab API to get the list of open merge request assigned to the specified user.
    """

    url = f"{BASE_URL}/merge_requests?state=opened&assignee_id={USER_ID}"
    headers = {"Authorization": f"Bearer {ACCESS_TOKEN}"}

    try:
        response = requests.get(url, headers=headers)
        response.raise_for_status()
        merge_requests = response.json()

        for mr in merge_requests:
            mr['project_name'] = get_project_name(mr['project_id'])

        return merge_requests

    except requests.exceptions.RequestException as e:
        print(f"Error fetching data: {e}")
        return []

def get_open_issues() -> list:
    """
    Fetch the gitlab API to get a list of all open issues assigned to the specified user.
    """

    # Fetch Issues assigned to the user
    url = f"{BASE_URL}/issues?scope=all&state=opened&assignee_id={USER_ID}"
    headers = {"Authorization": f"Bearer {ACCESS_TOKEN}"}

    try:
        response = requests.get(url, headers=headers)
        response.raise_for_status()
        issues = response.json()

        for issue in issues:
            issue['project_name'] = get_project_name(issue['project_id'])

        return issues

    except requests.exceptions.RequestException as e:
        print(f"Error fetching data: {e}")
        return []

def get_data() -> dict[list]:
    """
    Fetch the gitlab API to get a list of all open merge requests and issues assigned to the specificied user.
    """

    merge_requests = get_open_merge_requests()
    issues = get_open_issues()

    return {"merge_requests" : merge_requests,
            "issues" : issues}

def main():
    merge_requests = get_open_merge_requests()
    issues = get_open_issues()

    for k in merge_requests[0].keys():
        print(k)


if __name__ == "__main__":
    main()