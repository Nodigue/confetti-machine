#/usr/bin/env python3

import itertools

def compute_z2(z1 : int, distance : float, module : int):
    return z1 + ((2 * distance) / module) 

def main() -> None:
    """
    Compute the number of teeth of the second gear
    """

    module_list = [1, 2, 3]
    distance = [40.92, 42.69]

    for a, m in itertools.product(distance, module_list):
        z2 = compute_z2(12, a, m)
        print(f"Distance : {a} / Module : {m} = {z2}")


if __name__ == '__main__':
    main()