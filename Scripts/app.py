#!/usr/bin/env python3

from flask import Flask, render_template
from gitlab_connexion import *


# Improvement use a webhook instead
app = Flask(__name__,
            static_folder='static',
            template_folder='templates')

@app.route("/")
def index():
    return render_template("index.html", user='NRA')

@app.route("/data")
def data():
    return get_data()

if __name__ == "__main__":
    app.run(debug=True)