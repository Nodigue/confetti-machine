function updateOpenMergeRequestsTable(merge_requests) {
    const table = document.getElementById("open_merge_requests_table_body");
    table.innerHTML = "";

    merge_requests.forEach(mr => {
        const row = document.createElement("tr");

        const titleCell = document.createElement("td");
        const titleLink = document.createElement("a");
        titleLink.textContent = mr.title;
        titleLink.href = mr.web_url;
        titleLink.target = "_blank";
        titleCell.appendChild(titleLink);
        row.appendChild(titleCell);

        const projectCell = document.createElement("td");
        projectCell.textContent = mr.project_name
        row.appendChild(projectCell);

        table.appendChild(row);
    });
}

function updateOpenIssuesTable(issues) {
    const table = document.getElementById("open_issues_table_body");
    table.innerHTML = "";

    issues.forEach(issue => {
        const row = document.createElement("tr");

        const titleCell = document.createElement("td");
        const titleLink = document.createElement("a");
        titleLink.textContent = issue.title;
        titleLink.href = issue.web_url;
        titleLink.target = "_blank";
        titleCell.appendChild(titleLink);
        row.appendChild(titleCell);

        const projectCell = document.createElement("td");
        projectCell.textContent = issue.project_name
        row.appendChild(projectCell);

        table.appendChild(row);
    });
}

function fetchDataAndUpdateTables() {
    fetch("/data")
        .then(response => response.json())
        .then(data => {
            updateOpenMergeRequestsTable(data.merge_requests);
            updateOpenIssuesTable(data.issues);
        })
        .catch(error => console.error("Error fetching data:", error));
}

fetchDataAndUpdateTables();
setInterval(fetchDataAndUpdateTables, 30000); // Poll every 30 seconds
