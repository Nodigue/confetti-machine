# Confetti Machine

<img src="Images/photoview360_B0yStfr8rZ.png" height="400" />
<img src="Images/ApplicationFrameHost_zNhED0jVZz.gif" height="400" />

## Motivation

Bored of having to wait days to get your code merged, with this **over-engineered** confetti machine, when your MR will be merged, you'll be able to regain some joy and celebrate it with some confetti !

## Conception

This machine was designed with the only purpose to throw confetti. Even if the machine, looks complicated, the concept is simple ! Two motors rotate to **stretch a balloon** by operating a **lift mechanism**. The arms of the motors will pick up the elevator at the top which will pull on a rope and stretch the balloon. The lift will be released automatically at the bottom and confetti will be thrown out.

Unfortunatly, currently, confetti have to be placed by hand in the ballon.

Finally, thanks to a connexion to the Gitlab API, the confetti machine will be automatically triggered when your assigned MR are merged.

The whole CAD, electronic board and programming was done from scratch by myself.

## CAD

The CAD was done using **Solidworks 2021 SP4.0**

<img src="Images/SLDWORKS_p9XZTrHRyd.png" width="500" />

Then all the parts was **3D printed** with the exception of, of courses, the motors (two MG996 360° servomotors), the bearing, the aluminim rods, the ballon and the screws and bolts.

Two FDM 3D printers were used:
- Creality Ender 3 pro
- Creality CR10 Smart Pro

The STL files were sliced using **Cura** and print with PLA.

## Electronics

The electronic board was soldered by hand on a prototype board. It contains two slots for the servomotors connection, two LEDs (one red and one green), a button, a power supply connexion and an arduino nano.

<img src="Images/IMG_20230518_142320.jpg" height="400" />
<img src="Images/IMG_20230518_142326.jpg" height="400" />

I am currently working on making a proper PCB

## Firmware / Software

Currently the press of the button will make the motors turn simultaneously. A simple arduino program was enough.